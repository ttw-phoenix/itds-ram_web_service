
package gov.dhs.cbp.iws.itdsservices.iws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumServiceStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumServiceStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SUCCESS"/>
 *     &lt;enumeration value="FAILURE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumServiceStatus")
@XmlEnum
public enum EnumServiceStatus {

    SUCCESS,
    FAILURE;

    public String value() {
        return name();
    }

    public static EnumServiceStatus fromValue(String v) {
        return valueOf(v);
    }

}
