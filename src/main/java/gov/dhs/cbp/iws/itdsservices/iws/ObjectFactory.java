
package gov.dhs.cbp.iws.itdsservices.iws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.dhs.cbp.iws.itdsservices.iws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CredentialData_QNAME = new QName("http://iws.cbp.dhs.gov/ITDSServices/IWS", "CredentialData");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.dhs.cbp.iws.itdsservices.iws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CredentialData }
     * 
     * @return 
     */
    public CredentialData createCredentialData() {
        return new CredentialData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CredentialData }{@code >}}
     * 
     * @param value
     * @return 
     */
    @XmlElementDecl(namespace = "http://iws.cbp.dhs.gov/ITDSServices/IWS", name = "CredentialData")
    public JAXBElement<CredentialData> createCredentialData(CredentialData value) {
        return new JAXBElement<>(_CredentialData_QNAME, CredentialData.class, null, value);
    }

}
