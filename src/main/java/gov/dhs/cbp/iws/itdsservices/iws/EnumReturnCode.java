
package gov.dhs.cbp.iws.itdsservices.iws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumReturnCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumReturnCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SUCCESS"/>
 *     &lt;enumeration value="FAILURE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumReturnCode")
@XmlEnum
public enum EnumReturnCode {

    SUCCESS,
    FAILURE;

    public String value() {
        return name();
    }

    public static EnumReturnCode fromValue(String v) {
        return valueOf(v);
    }

}
