/**
 * 
 *            Service Name: ReferenceDataService
 * 		   Purpose:  ITDS (IWS) WSDL to enable PGAs manage their Reference Data maintained in CBP. To be implemented and hosted by CBP.
 * 		   Created by TASPO in collaboration with ITDS PGA Participants for ACE IWS
 * 			  POC: Shailesh Sardesai, 703-553-6633, shailesh.s.sardesai@PGA.dhs.gov
 * 			  POC: Ed Jenkins, 703-553-1722, edward.n.jenkins@PGA.dhs.gov
 * 			  POC: Jim Mclaughlin, 703-553-1767, jim.e.mclaughlin@PGA.dhs.gov
 * 		   WSDL Specification: 1.1
 * 		   IWS Spec version: 2.0
 * 		   Usage: ITDS (IWS) WSDL to enable PGAs manage their Reference Data maintained in CBP
 * 		   Note: Arguments to operations are deliberately defined as strings rather than XML types to minimize code impact to top-down implementations when complex types change.
 *    
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://iws.cbp.dhs.gov/ITDSServices/IWS", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package gov.dhs.cbp.iws.itdsservices.iws;
