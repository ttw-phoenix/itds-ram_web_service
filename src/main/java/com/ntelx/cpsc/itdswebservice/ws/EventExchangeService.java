package com.ntelx.cpsc.itdswebservice.ws;

import com.ntelx.cpsc.itdswebservice.config.AppConfig;
import com.ntelx.cpsc.itdswebservice.services.SecurityTokenService;
import com.ntelx.cpsc.itdswebservice.services.WriteDataService;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.soap.SOAPBinding;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rzauel
 */
@WebService(serviceName = "EventExchangeService", portName = "EventExchangeServiceEndPoint", endpointInterface = "gov.dhs.cbp.iws.itdsservices.iws.EventExchangeServicePortType", targetNamespace = "http://iws.cbp.dhs.gov/ITDSServices/IWS", wsdlLocation = "WEB-INF/wsdl/ITDS_EventExchangeService.wsdl")
@BindingType(SOAPBinding.SOAP12HTTP_BINDING)
public class EventExchangeService {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(EventExchangeService.class);
    private static final long TEN_MINUTES = 600000L; // 10 minutes * 60 seconds / minute * 1000 seconds / millisecond
    private static Timer t;

    public EventExchangeService() {
        t = new Timer();
        t.scheduleAtFixedRate(
                new TimerTask() {
                    @Override
                    public void run() {
                        SecurityTokenService securityTokenService = SecurityTokenService.getInstance();
                        securityTokenService.purgeOldTokens();
                    }
                },
                TEN_MINUTES, // run first occurrence in ten minutes
                TEN_MINUTES);  // run every 10 minutes 
    }

    public java.lang.String getSecurityToken(gov.dhs.cbp.iws.itdsservices.iws.CredentialData credentialData) {
        log.info("getSecurityToken called by " + credentialData.getUserID());
        SecurityTokenService securityTokenService = SecurityTokenService.getInstance();
        String token = securityTokenService.getSecurityToken(credentialData);
        if (token != null) {
            log.debug("getSecurityToken valid");
            return token;
        }
        log.info("getSecurityToken failure invalid");
        return AppConfig.FAILURE_INVALID_CREDENTIALS;
    }

    public java.lang.String checkServiceStatus(java.lang.String securityToken) {
        log.debug("checkServiceStatus called");
        SecurityTokenService securityTokenService = SecurityTokenService.getInstance();
        if (securityTokenService.checkSecurityToken(securityToken)) {
            log.debug("checkServiceStatus return valid");
            return AppConfig.SUCCESS_SECURITY_TOKEN_VALID;
        }
        log.info("checkServiceStatus return invalid");
        return AppConfig.FAILURE_INVALID_SECURITY_TOKEN;
    }

    public java.lang.String submitEventNotification(java.lang.String securityToken, java.lang.String eventDataPackage) {
        log.debug("submitEventNotification called");
        String rtn;
        SecurityTokenService securityTokenService = SecurityTokenService.getInstance();
        if (securityTokenService.checkSecurityToken(securityToken)) {
            try {
                WriteDataService writeDataService = WriteDataService.getInstance();
                writeDataService.writeData(eventDataPackage);
                log.debug("submitEventNotification Success");
                rtn = AppConfig.SUCCESS_EVENT_NOTIFICATION;
            } catch (IOException e) {
                log.error("unable to write event data to file", e);
                log.info("submitEventNotification System Failure");
                rtn = AppConfig.FAILURE_SYSTEM_ISSUE;
            }
        } else {
            log.info("submitEventNotification invalid security token");
            rtn = AppConfig.FAILURE_INVALID_SECURITY_TOKEN;
        }
        return rtn;
    }

}
