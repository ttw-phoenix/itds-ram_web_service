package com.ntelx.cpsc.itdswebservice.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rzauel
 *
 * Singleton
 *
 */
public class AppConfig {

    private static final Logger log = LoggerFactory.getLogger(AppConfig.class);

    // Singleton
    private static AppConfig instance = null;

    public static final String SUCCESS_SECURITY_TOKEN_VALID = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<IWS:ResponseMessage xsi:schemaLocation=\"http://iws.cbp.dhs.gov/ITDSServices/IWS ResponseMessage.xsd\" "
            + "xmlns:IWS=\"http://iws.cbp.dhs.gov/ITDSServices/IWS\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
            + "	<IWS:ReturnCode>SUCCESS</IWS:ReturnCode>\n"
            + "	<IWS:ReturnRemark>Security Token Is Valid</IWS:ReturnRemark>\n"
            + "</IWS:ResponseMessage>";

    public static final String SUCCESS_EVENT_NOTIFICATION = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<IWS:ResponseMessage xsi:schemaLocation=\"http://iws.cbp.dhs.gov/ITDSServices/IWS ResponseMessage.xsd\" "
            + "xmlns:IWS=\"http://iws.cbp.dhs.gov/ITDSServices/IWS\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
            + "	<IWS:ReturnCode>SUCCESS</IWS:ReturnCode>\n"
            + "	<IWS:ReturnRemark>submitEventNotification was successful</IWS:ReturnRemark>\n"
            + "</IWS:ResponseMessage>";

    public static final String FAILURE_INVALID_CREDENTIALS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<IWS:ResponseMessage xsi:schemaLocation=\"http://iws.cbp.dhs.gov/ITDSServices/IWS ResponseMessage.xsd\" "
            + "xmlns:IWS=\"http://iws.cbp.dhs.gov/ITDSServices/IWS\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
            + "	<IWS:ReturnCode>FAILURE</IWS:ReturnCode>\n"
            + "	<IWS:ReturnRemark>Invalid Credentials</IWS:ReturnRemark>\n"
            + "	<IWS:ErrorDetailsList>\n"
            + "		<IWS:ErrorDetails>\n"
            + "			<IWS:ErrorCode>1000</IWS:ErrorCode>\n"
            + "			<IWS:ErrorDescription>Invalid Credentials</IWS:ErrorDescription>\n"
            + "		</IWS:ErrorDetails>\n"
            + "	</IWS:ErrorDetailsList>\n"
            + "</IWS:ResponseMessage>";

    public static final String FAILURE_INVALID_SECURITY_TOKEN = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<IWS:ResponseMessage xsi:schemaLocation=\"http://iws.cbp.dhs.gov/ITDSServices/IWS ResponseMessage.xsd\" "
            + "xmlns:IWS=\"http://iws.cbp.dhs.gov/ITDSServices/IWS\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
            + "	<IWS:ReturnCode>FAILURE</IWS:ReturnCode>\n"
            + "	<IWS:ReturnRemark>Invalid Security Token</IWS:ReturnRemark>\n"
            + "	<IWS:ErrorDetailsList>\n"
            + "		<IWS:ErrorDetails>\n"
            + "			<IWS:ErrorCode>1001</IWS:ErrorCode>\n"
            + "			<IWS:ErrorDescription>Invalid Security Token</IWS:ErrorDescription>\n"
            + "		</IWS:ErrorDetails>\n"
            + "	</IWS:ErrorDetailsList>\n"
            + "</IWS:ResponseMessage>";

    public static final String FAILURE_SYSTEM_ISSUE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<IWS:ResponseMessage xsi:schemaLocation=\"http://iws.cbp.dhs.gov/ITDSServices/IWS ResponseMessage.xsd\" "
            + "xmlns:IWS=\"http://iws.cbp.dhs.gov/ITDSServices/IWS\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
            + "	<IWS:ReturnCode>FAILURE</IWS:ReturnCode>\n"
            + "	<IWS:ReturnRemark>Cpsc system is having difficulty processing request</IWS:ReturnRemark>\n"
            + "	<IWS:ErrorDetailsList>\n"
            + "		<IWS:ErrorDetails>\n"
            + "			<IWS:ErrorCode>1003</IWS:ErrorCode>\n"
            + "			<IWS:ErrorDescription>Cpsc system is having difficulty processing request</IWS:ErrorDescription>\n"
            + "		</IWS:ErrorDetails>\n"
            + "	</IWS:ErrorDetailsList>\n"
            + "</IWS:ResponseMessage>";

    public static final String SYSTEM_PROPERTY_FILE = "itdsWebServicePropertyFile";
    private Properties appProperties;

    // Keys in property file
    public static final String TRIPLE_DES_KEY = "triple.des.key";
    public static final String USER_ID = "user.id";
    public static final String PASSWORD = "password";
    public static final String SECURITY_TOKEN_LEN = "security.token.length";
    public static final String SECURITY_TOKEN_TIMEOUT = "security.token.timeout";
    public static final String DIR_TO_WRITE_TO = "dir.to.write.to";
    public static final String FILENAME_PREFIX = "filename.prefix";
    public static final String FILENAME_SUFFIX = "filename.suffix";

    protected AppConfig() {
        // Exists only to defeat instantiation.
    }

    public static AppConfig getInstance() {
        if (instance == null) {
            instance = new AppConfig();
            instance.initialize();
        }
        return instance;
    }

    private void initialize() {
        appProperties = new Properties();
        String propertyFilePath = null;
        try {
            propertyFilePath = System.getProperty(SYSTEM_PROPERTY_FILE);
            //appProperties.load(this.getClass().getResourceAsStream(propertyFilePath));
            appProperties = loadProperties(propertyFilePath);
        } catch (IOException ex) {
            log.error("error loading property file: " + propertyFilePath, ex);
        }
    }

    private Properties loadProperties(final String propertiesFile)
            throws IOException {
        // Properties object we are going to fill up.
        Properties properties = new Properties();
        // If file exists as an absolute path, load as input stream.
        final Path path = Paths.get(propertiesFile);
        if (Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
            properties.load(new FileInputStream(propertiesFile));
        } else {
            // Otherwise, use resource as stream.
            properties.load(getClass().getClassLoader().getResourceAsStream(
                    propertiesFile));
        }
        return properties;
    }

    public Properties getProperties() {
        return appProperties;
    }

    public static String getProperty(String key) {
        AppConfig appConfig = AppConfig.getInstance();
        Properties properties = appConfig.getProperties();
        String rtn = properties.getProperty(key);
        if (rtn == null) {
            log.error("property not found: " + key);
        }
        return rtn;
    }

    public static int getPropertyInt(String key) {
        int rtn = 0;
        AppConfig appConfig = AppConfig.getInstance();
        Properties properties = appConfig.getProperties();
        String rtnStr = properties.getProperty(key);
        if (rtnStr == null) {
            log.error("property not found: " + key);
        } else {
            try {
                rtn = Integer.parseInt(rtnStr);
            } catch (NumberFormatException e) {
                log.error("property not found: " + key, e);
            }
        }
        return rtn;
    }

}
