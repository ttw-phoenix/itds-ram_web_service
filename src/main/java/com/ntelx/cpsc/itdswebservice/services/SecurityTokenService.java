package com.ntelx.cpsc.itdswebservice.services;

import com.ntelx.cpsc.itdswebservice.common.SecurityTokenGenerator;
import com.ntelx.cpsc.itdswebservice.config.AppConfig;
import gov.dhs.cbp.iws.itdsservices.iws.CredentialData;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rzauel
 */
public class SecurityTokenService {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SecurityTokenService.class);

    private static final String storedUserId = AppConfig.getProperty(AppConfig.USER_ID);
    private static final String storedPassword = AppConfig.getProperty(AppConfig.PASSWORD);
    private static final int timeoutMinutes = AppConfig.getPropertyInt(AppConfig.SECURITY_TOKEN_TIMEOUT);
    private static final Map<String, Date> securityTokenMap = new HashMap<>();
    private static final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs

    // Singleton
    private static SecurityTokenService instance = null;

    protected SecurityTokenService() {
        // Exists only to defeat instantiation.
    }

    public static SecurityTokenService getInstance() {
        if (instance == null) {
            instance = new SecurityTokenService();
        }
        return instance;
    }

    public String getSecurityToken(CredentialData credentialData) {
        String userId = credentialData.getUserID();
        String password = credentialData.getPassword();
        String securityToken = null;
        if (StringUtils.isNotBlank(userId)
                && StringUtils.isNotBlank(password)) {
            if (storedUserId.equals(userId)
                    && storedPassword.equals(password)) {
                securityToken = SecurityTokenGenerator.createSecurityToken();
            }
        }
        if (securityToken != null) {
            Date expireDate = minutesFromNow(timeoutMinutes);
            securityTokenMap.put(securityToken, expireDate);
        }
        return securityToken;
    }

    private static Date minutesFromNow(int minutes) {
        Date nowDate = new Date();
        long nowLong = nowDate.getTime();
        Date afterAddingMinutes = new Date(nowLong + (ONE_MINUTE_IN_MILLIS * minutes));
        return afterAddingMinutes;
    }

    public boolean checkSecurityToken(String securityToken) {
        boolean rtn = false;
        if (securityTokenMap.containsKey(securityToken)) {
            Date expireDate = securityTokenMap.get(securityToken);
            Date now = new Date();
            if (now.before(expireDate)) {
                rtn = true;
            } 
        }
        return rtn;
    }

    public void purgeOldTokens() {
        log.info("Purging Security Tokens.");
        Date dtNow = new Date();
        int mapSize = securityTokenMap.size();
        int numActive = 0;
        int numPurged = 0;
        Iterator<Map.Entry<String, Date>> it = securityTokenMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Date> pairs = it.next();
            Date expireDate = pairs.getValue();
            if (expireDate.before(dtNow)) {
                numPurged++;
                it.remove(); // avoids a ConcurrentModificationException
            } else {
                numActive++;
            }
        }
        log.info("Purge: active tokens: " + numActive + " purged tokens: " + numPurged);
        log.info("Purge: original list size: " + mapSize + " new list size: " + securityTokenMap.size());
    }
}
