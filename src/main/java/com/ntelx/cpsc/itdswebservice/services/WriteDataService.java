package com.ntelx.cpsc.itdswebservice.services;

import com.ntelx.cpsc.itdswebservice.config.AppConfig;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rzauel
 */
public class WriteDataService {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(WriteDataService.class);

    private static final String rootDir = AppConfig.getProperty(AppConfig.DIR_TO_WRITE_TO);

    // Singleton
    private static WriteDataService instance = null;

    protected WriteDataService() {
        // Exists only to defeat instantiation.
    }

    public static WriteDataService getInstance() {
        if (instance == null) {
            instance = new WriteDataService();
        }
        return instance;
    }

    public void writeData(String eventDataPackage) throws IOException {
        String fileNameStr = AppConfig.getProperty(AppConfig.FILENAME_PREFIX) + getTimestampString() + AppConfig.getProperty(AppConfig.FILENAME_SUFFIX);
        String parentDirStr = rootDir + File.separator + getDateString();
        String fullFilePathStr = parentDirStr + File.separator + fileNameStr;
        Path parentDirPath = Paths.get(parentDirStr);
        if (!Files.exists(parentDirPath)) {
            Files.createDirectories(parentDirPath);
        }

        Files.write(Paths.get(fullFilePathStr), eventDataPackage.getBytes());
        log.info("wrote to file: " + fullFilePathStr);
    }

    private static String getDateString() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("MM-dd-yyyy");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    private static String getTimestampString() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

}
