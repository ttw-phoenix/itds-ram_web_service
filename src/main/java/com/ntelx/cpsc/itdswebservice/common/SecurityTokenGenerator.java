package com.ntelx.cpsc.itdswebservice.common;

import com.ntelx.cpsc.itdswebservice.config.AppConfig;
import java.security.SecureRandom;

/**
 *
 * @author rzauel
 */
public class SecurityTokenGenerator {
    
    public static String createSecurityToken() {
        String securityTokenLen = AppConfig.getProperty(AppConfig.SECURITY_TOKEN_LEN);
        int tokenLen = Integer.parseInt(securityTokenLen);
        return SecurityTokenGenerator.createSecurityToken(tokenLen);
    }

    /**
     * Creates a Random Token String of TOKEN_LENGTH using only
     * Alpha (Upper and Lower) and Digits
     * @param tokenLength
     * @return
     */
    public static String createSecurityToken(int tokenLength) {
        char[] charAry = new char[tokenLength];
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < tokenLength; i++) {
            // Random Number between 0 and 61
            int randomNum = random.nextInt(62);
            if (randomNum < 26) {
                // 0 - 25 => 'A' to 'Z'
                charAry[i] = (char) ('A' + randomNum);
            } else if (randomNum < 52) {
                // 26 - 51 => 'a' to 'z'
                charAry[i] = (char) ('a' + randomNum - 26);
            } else {
                // 52 - 61 => '0' to '9'
                charAry[i] = (char) ('0' + randomNum - 52);
            }
        }
        String rtn = new String(charAry);
        return rtn;
    }
}
