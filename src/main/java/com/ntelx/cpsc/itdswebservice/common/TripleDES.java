package com.ntelx.cpsc.itdswebservice.common;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author rzauel
 * 
 * Using concepts discussed on stack overflow: {@link http://stackoverflow.com/questions/20227/how-do-i-use-3des-encryption-decryption-in-java}.
 * 
 */
public class TripleDES {

    public static byte[] encrypt(String tripleDesKey, String message) throws Exception {
        final MessageDigest md = MessageDigest.getInstance("md5");
        final byte[] digestOfKey = md.digest(tripleDesKey.getBytes("utf-8"));
        final byte[] keyBytes = Arrays.copyOf(digestOfKey, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }
        final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
        final byte[] plainTextBytes = message.getBytes("utf-8");
        final byte[] cipherText = cipher.doFinal(plainTextBytes);
        return cipherText;
    }

    public static String decrypt(String tripleDesKey, byte[] message) throws Exception {
        final MessageDigest md = MessageDigest.getInstance("md5");
        final byte[] digestOfKey = md.digest(tripleDesKey.getBytes("utf-8"));
        final byte[] keyBytes = Arrays.copyOf(digestOfKey, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }
        final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        final Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        decipher.init(Cipher.DECRYPT_MODE, key, iv);
        final byte[] plainText = decipher.doFinal(message);
        return new String(plainText, "UTF-8");
    }
}
