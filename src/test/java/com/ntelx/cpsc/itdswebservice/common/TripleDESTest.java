/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ntelx.cpsc.itdswebservice.common;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rzauel
 */
public class TripleDESTest {
    
    public TripleDESTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of encrypt and decrypt methods, of class TripleDES.
     * @throws java.lang.Exception
     */
    @Test
    public void testEncryptDecrypt() throws Exception {
        String text = "This is a test message";
        String key = "Re3Et46XaBf";
        byte[] codedtext = TripleDES.encrypt(key, text);
        String decodedtext = TripleDES.decrypt(key, codedtext);
        String codedTextStr = new String(codedtext);
        System.out.println("text:      " + text); 
        System.out.println("entrypted: " + codedTextStr); 
        System.out.println("decrypted: " + decodedtext);
        assertTrue(text.contentEquals(decodedtext));
    }

}
